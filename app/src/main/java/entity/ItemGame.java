package entity;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by asus on 10/11/16.
 */

public class ItemGame implements Parcelable {
    public ItemGame() {

    }

    public String getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getImg() {
        return img;
    }

    public String getDev() {
        return dev;
    }

    public String getDesc() {
        return desc;
    }

    public String getDate() {
        return date;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setDev(String dev) {
        this.dev = dev;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String id;
    private String category;
    private String img;
    private String name;
    private String dev;
    private String desc;
    private String date;

    public float getRate() {
        return rate;
    }
    public void setRate(float rate) {
        this.rate = rate;
    }

    private float rate;
    private String picture[];

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    private Bitmap bitmap;


    public ItemGame(String id, String category, String img, String name, String dev, String desc, String date) {
        this.id = id;
        this.category = category;
        this.img = img;
        this.name = name;
        this.dev = dev;
        this.desc = desc;
        this.date = date;
        this.picture = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getPicture() {
        return picture;
    }

    public void setPicture(String[] picture) {
        this.picture = picture;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(category);
        parcel.writeString(img);
        parcel.writeString(name);
        parcel.writeString(dev);
        parcel.writeString(desc);
        parcel.writeString(date);
        parcel.writeFloat(rate);
        parcel.writeStringArray(picture);

    }

    private ItemGame(Parcel p) {
        this.id = p.readString();
        this.category = p.readString();
        this.img = p.readString();
        this.name = p.readString();
        this.dev = p.readString();
        this.desc = p.readString();
        this.date = p.readString();
        this.rate = p.readFloat();
        this.picture = p.createStringArray();
    }

    public static final Parcelable.Creator<ItemGame> CREATOR = new Parcelable.Creator<ItemGame>() {

        @Override
        public ItemGame createFromParcel(Parcel source) {
            return new ItemGame(source);
        }

        @Override
        public ItemGame[] newArray(int size) {
            return new ItemGame[size];
        }
    };
}
