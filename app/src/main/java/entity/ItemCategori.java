package entity;

/**
 * Created by asus on 10/11/16.
 */

public class ItemCategori {

    private String name;
    private int idcategori;

    public ItemCategori(String name, int idcategori) {
        this.name = name;
        this.idcategori = idcategori;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdcategori() {
        return idcategori;
    }

    public void setIdcategori(int idcategori) {
        this.idcategori = idcategori;
    }
}
