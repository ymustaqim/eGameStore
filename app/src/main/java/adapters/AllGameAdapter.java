package adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import egames.egamesstore.R;
import entity.ItemGame;

/**
 * Created by asus on 10/11/16.
 */

public class AllGameAdapter extends RecyclerView.Adapter<AllGameHolders> {

    private List<ItemGame> itemList;
    private Context context;

    public AllGameAdapter(Context context, List<ItemGame> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public AllGameHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate the layout
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_card, null);
        AllGameHolders rcv = new AllGameHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(AllGameHolders holder, int position) {
        holder.gameTitle.setText(itemList.get(position).getName());
        //holder.gamePicture.setImageResource(itemList.get(position).getPicture());
        Log.d("Gambar daftar e", "http:" + itemList.get(position).getImg());
        //holder.gamePicture.setImageResource(R.drawable.common_ic_googleplayservices);
        holder.gamePicture.setImageBitmap(itemList.get(position).getBitmap());
        holder.itemGame = itemList.get(position);
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void clearData() {
        int size = this.itemList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.itemList.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addItemList (List<ItemGame> list){
        int curSize = itemList.size();
        itemList.addAll(list);
        this.notifyItemRangeInserted(curSize, itemList.size()-1);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        private ImageView img;
        InputStream in = null;
        BufferedOutputStream out = null;

        public DownloadImageTask(ImageView img) {
            this.img = img;
        }

        @Override
        protected Bitmap doInBackground(String... url) {

            // make connection to url
            String imgUrl = url[0];
            Bitmap pict = null;
            try {
                in = new URL(imgUrl).openConnection().getInputStream();
                pict = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Log.d("nama gambar e", imgUrl);
            Log.d("Hasil e", pict.toString());

            // set image to bitmap
            return pict;

        }

        protected void OnPostExecute(Bitmap res) {

            img.setImageBitmap(res);
        }
    }
}
