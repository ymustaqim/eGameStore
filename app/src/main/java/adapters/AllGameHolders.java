package adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import egames.egamesstore.DetailGame;
import egames.egamesstore.R;
import entity.ItemGame;


/**
 * Created by asus on 10/11/16.
 */

public class AllGameHolders extends RecyclerView.ViewHolder implements View.OnClickListener  {
    public TextView gameTitle;
    public ImageView gamePicture;
    public ItemGame itemGame;

    public AllGameHolders(View itemView) {
        super(itemView);

        itemGame = null;

        gameTitle = (TextView) itemView.findViewById(R.id.gameTitle);
        gamePicture = (ImageView) itemView.findViewById(R.id.gamePicture);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick (View view) {
        Intent intent = new Intent(view.getContext(), DetailGame.class);
        intent.putExtra("itemGame", itemGame);
        intent.putExtra("gamePicture", itemGame.getBitmap());
        view.getContext().startActivity(intent);

    }
}
