package adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import egames.egamesstore.DetailGame;
import egames.egamesstore.R;
import entity.ItemGame;


/**
 * Created by asus on 10/11/16.
 */

public class GameListHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
    public TextView titleDetail;
    public ImageView imgDetail;
    public RatingBar ratingBar;
    public ItemGame itemGame;
    public ImageButton imageButton;
    public View view;

    public GameListHolder(View itemView) {
        super(itemView);

        itemGame = null;

        titleDetail = (TextView) itemView.findViewById(R.id.titleDetail);
        imgDetail = (ImageView) itemView.findViewById(R.id.imgDetail);
        ratingBar = (RatingBar) itemView.findViewById(R.id.gameRating);
        imageButton = (ImageButton) itemView.findViewById(R.id.detailGame);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick (View view) {
        Intent intent = new Intent(view.getContext(), DetailGame.class);
        intent.putExtra("itemGame", itemGame);
        intent.putExtra("gamePicture", itemGame.getBitmap());
        view.getContext().startActivity(intent);

    }
}
