package adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import database.dbhelper;
import egames.egamesstore.R;
import egames.egamesstore.Wishlist;
import entity.ItemGame;

/**
 * Created by asus on 10/11/16.
 */

public class RatingAdapater extends RecyclerView.Adapter<RatingHolder> {

    private List<ItemGame> itemList;
    private Context context;
    private boolean onWishlist;

    public RatingAdapater(Context context, List<ItemGame> itemList, boolean onWishlist) {
        this.itemList = itemList;
        this.context = context;
        this.onWishlist = onWishlist;

    }
    public RatingAdapater(Context context, List<ItemGame> itemList) {
        this.itemList = itemList;
        this.context = context;
        this.onWishlist = false;
    }

    @Override
    public RatingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate the layout
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.best_rating_item, null);

        RatingHolder rcv = new RatingHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RatingHolder holder, int position) {
        holder.titleDetail.setText(itemList.get(position).getName());
        Log.d("Nama game e",itemList.get(position).getName());
        //holder.gamePicture.setImageResource(itemList.get(position).getPicture());
        Log.d("Gambar daftar e", "http:" + itemList.get(position).getImg());
        //holder.gamePicture.setImageResource(R.drawable.common_ic_googleplayservices);
        holder.imgDetail.setImageBitmap(itemList.get(position).getBitmap());
        holder.ratingBar.setRating(itemList.get(position).getRate());
        holder.itemGame = itemList.get(position);
        final ItemGame ig = itemList.get(position);

        holder.imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
                if (onWishlist){
                    popupMenu.getMenuInflater().inflate(R.menu.menu_wishlist, popupMenu.getMenu());
                }else{
                    popupMenu.getMenuInflater().inflate(R.menu.menu_app, popupMenu.getMenu());
                }

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        dbhelper db = new dbhelper(view.getContext());
                        if (item.getItemId()==R.id.add_to_wishlist){


                            if (!db.checkRow(ig.getId())) {
                                db.insert(ig);
                                Toast.makeText(view.getContext(), ig.getName() + " successfully added to wishlist", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(view.getContext(), ig.getName() + " already exists in wishlist", Toast.LENGTH_SHORT).show();
                            }
                        }else if(item.getItemId()==R.id.remove_from_wishlist){
                            db.delete(ig.getId());
                            Toast.makeText(view.getContext(), ig.getName() + " successfully removed from wishlist", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RatingAdapater.this.context, Wishlist.class);
                            RatingAdapater.this.context.startActivity(intent);

                        }
                        return true;
                    }
                });

                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void clearData() {
        int size = this.itemList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.itemList.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addItemList (List<ItemGame> list){
        int curSize = itemList.size();
        itemList.addAll(list);
        this.notifyItemRangeInserted(curSize, itemList.size()-1);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        private ImageView img;
        InputStream in = null;
        BufferedOutputStream out = null;

        public DownloadImageTask(ImageView img) {
            this.img = img;
        }

        @Override
        protected Bitmap doInBackground(String... url) {

            // make connection to url
            String imgUrl = url[0];
            Bitmap pict = null;
            try {
                in = new URL(imgUrl).openConnection().getInputStream();
                pict = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Log.d("nama gambar e", imgUrl);
            Log.d("Hasil e", pict.toString());

            // set image to bitmap
            return pict;

        }

        protected void OnPostExecute(Bitmap res) {

            img.setImageBitmap(res);
        }
    }
}
