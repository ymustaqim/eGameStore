package adapters;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by asus on 14/11/16.
 */

public class WebServices{
    public static String HTTP_URL = "http://103.43.47.115:8080/";
    public static int HTTP_GET = 1;
    public static int HTTP_POST = 2;


    public String GetDataFromService(String subURL, int HTTPMethod, HashMap<String, String> params) throws IOException {

        URL url = new URL(HTTP_URL+subURL);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();


        if (HTTPMethod == HTTP_POST) {
            connection.setDoOutput(true);
            connection.setChunkedStreamingMode(0);

            StringBuilder sb = new StringBuilder();
            if (params != null) {
                OutputStream os = new BufferedOutputStream(connection.getOutputStream());
                OutputStreamWriter writer = new OutputStreamWriter(os);

                Iterator iterator = params.entrySet().iterator();

                while (iterator.hasNext()) {
                    Map.Entry pair = (Map.Entry) iterator.next();

                    sb.append(pair.getKey() + "=" + pair.getValue());
                    if (!iterator.hasNext()) {
                        sb.append('&');
                    }
                }

                writer.write(sb.toString());
                writer.close();

            }




        }

        InputStream is = new BufferedInputStream(connection.getInputStream());
        InputStreamReader inputStreamReader = new InputStreamReader(is);

        StringBuilder res = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(inputStreamReader);

        while ((line = br.readLine()) != null) {
            res.append(line).append('\n');
        }

        connection.disconnect();

        return res.toString();

    }


}
