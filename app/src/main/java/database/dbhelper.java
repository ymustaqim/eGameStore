package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import entity.ItemGame;

/**
 * Created by asus on 13/12/16.
 */

public class dbhelper extends SQLiteOpenHelper{
    private final static String DATABASE_NAME = "wishlist.db";
    private final String TABLE_NAME = "wishlist";
    private final static int DATABASE_VERSION = 1;
    private SQLiteDatabase sqLiteDatabase;

    private final static String COLUMN_ID = "id";
    private final static String COLUMN_CATEGORY = "category";

    private Context context;
    public dbhelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "CREATE TABLE IF NOT EXISTS "+TABLE_NAME+" ("+COLUMN_ID+" TEXT PRIMARY KEY not null, "+
                COLUMN_CATEGORY+" TEXT not null)";
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS"+TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void delete (String id){
        try {
            SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
            sqLiteDatabase.delete(TABLE_NAME, COLUMN_ID+"=?", new String[]{id});
        }
        catch (Exception ex){
            Log.d("HAPUS", ex.getMessage());
        }
    }

    public boolean checkRow(String id){
        String query = null;
        query = "SELECT * FROM "+TABLE_NAME+" WHERE "+COLUMN_ID+"='"+id+"'";
        List<ItemGame> list = new ArrayList<>();
        sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if (cursor.getCount()>0){
            cursor.close();
            return true;
        }
        else{
            cursor.close();
            return false;
        }
    }

    public void insert (ItemGame itemGame){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_ID, itemGame.getId());
        contentValues.put(COLUMN_CATEGORY, itemGame.getCategory());
        sqLiteDatabase.insert(TABLE_NAME, null, contentValues);

    }

    public List<ItemGame> getData(){
        String query = null;
        query ="SELECT * FROM "+TABLE_NAME+"";

        List<ItemGame> list = new ArrayList<>();
        sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        while (cursor.moveToNext()){
            ItemGame itemGame = new ItemGame();
            itemGame.setId(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
            itemGame.setCategory(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY)));
            list.add(itemGame);
        }
        cursor.close();
        sqLiteDatabase.close();
        return list;
    }

}
