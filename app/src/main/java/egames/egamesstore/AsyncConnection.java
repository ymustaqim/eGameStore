package egames.egamesstore;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import adapters.WebServices;

/**
 * Created by asus on 14/11/16.
 */

public class AsyncConnection extends AsyncTask<Void, Void, String> {


    @Override
    protected String doInBackground(Void... voids) {
        WebServices ws = new WebServices();
        String data = "";

        try {
            data = ws.GetDataFromService("category", WebServices.HTTP_GET, null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    protected void onProgressUpdate(Void... progress) {

    }

    protected void onPostExecute(String result) {
        //Log.d("called from home", result);
        int i;
        try {
            JSONArray jsonArray = new JSONArray(result);
            for (i=0; i < jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String id = jsonObject.getString("id");
                String category = jsonObject.getString("catogory");
                String name = jsonObject.getString("name");
        }
    }
        catch (JSONException e){
            Log.e("JSONException", "Error: " + e.toString());
        }
    }
}
