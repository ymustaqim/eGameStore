package egames.egamesstore;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import adapters.RatingAdapater;
import adapters.WebServices;
import database.dbhelper;
import entity.ItemGame;

public class Wishlist extends AppCompatActivity {
    private LinearLayoutManager lLayout;
    private List<ItemGame> rowListItem = null;
    private RecyclerView rView;
    private SwipeRefreshLayout refreshGameList;

    private RatingAdapater allGameAdapter;
    private String cat;

    private String squery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Wishlist");
        setSupportActionBar(toolbar);

        lLayout = new LinearLayoutManager(this);

        rView = (RecyclerView) findViewById(R.id.recAll);
        rView.setLayoutManager(lLayout);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        rView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));


        refreshGameList = (SwipeRefreshLayout) findViewById(R.id.refreshGameList);

        refreshGameList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadGameList();
            }
        });

        refreshGameList.setRefreshing(true);

        //new AsyncConnection().execute();


//        Log.d("Unit Testing", intent.getAction());




            new GetGameListTask().execute();


    }
    private void reloadGameList() {
        allGameAdapter.clearData();
        new GetGameListTask().execute();
    }

    public class GetGameListTask extends AsyncTask<Void, Void, List<ItemGame>> {


        @Override
        protected List<ItemGame> doInBackground(Void... voids) {

            String iresponse = "";
            ArrayList<ItemGame> listGame = new ArrayList<ItemGame>();
            JSONObject rootObj = null;
            JSONArray listData = null;

            // make connection
            WebServices ws = new WebServices();
            dbhelper db = new dbhelper(Wishlist.this);
            List<ItemGame> lig = db.getData();
            for (ItemGame _ig:lig){
            Log.d("Data game gan", _ig.getId()+ " "+_ig.getCategory());
                try {
                    iresponse = ws.GetDataFromService("detail?id="+_ig.getId()+"&category="+_ig.getCategory(), WebServices.HTTP_GET, null);
                    Log.d("Res",iresponse);
                    try {
                        rootObj = new JSONObject(iresponse);
//                        JSONObject jd = rootObj.getJSONObject("hits");
//                        listData = jd.getJSONArray("hits");
//                        for (int i= 0; i < listData.length(); i++) {
                            if (rootObj.has("found")&&rootObj.getBoolean("found")) {
                                JSONObject g = null;
                                g = rootObj.getJSONObject("_source");
                                ItemGame ig;
                                ig = new ItemGame(g.getString("id").trim(),
                                        g.getString("category").trim(),
                                        g.getString("img").trim(),
                                        g.getString("name").trim(),
                                        g.getString("dev").trim(),
                                        g.getString("desc").trim(),
                                        g.getString("date").trim());
                                Bitmap gb = this.loadBitmap("http:" + g.getString("img"));
                                ig.setRate(Float.parseFloat("1"));
                                ig.setBitmap(gb);
                                listGame.add(ig);
                            }
//                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


            // return List<ItemGame>
            return listGame;
        }

        protected void onPostExecute(List<ItemGame> res) {

            // create adapter
            allGameAdapter = new RatingAdapater(Wishlist.this, res,true);
            // populate to recyclerview

            rView.setAdapter(allGameAdapter);
            refreshGameList.setRefreshing(false);

            // done.....
        }

        private Bitmap loadBitmap(String url) {
            InputStream in = null;
            Bitmap pict = null;
            try {
                in = new URL(url).openConnection().getInputStream();
                pict = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return pict;
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space){
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state){
            outRect.left= space;
            outRect.right = space;
            outRect.bottom = space;

            if (parent.getChildLayoutPosition(view)==0){
                outRect.top = space;
            }
            else{
                outRect.top=0;
            }

        }
    }


}
