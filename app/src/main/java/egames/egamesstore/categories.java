package egames.egamesstore;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import adapters.WebServices;

/**
 * Created by asus on 01/11/16.
 */

public class categories extends Fragment {

    List<String> item;
    ListView lv;
    View rootView;
    SwipeRefreshLayout refreshCategory;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.categories, container, false);

        lv = (ListView) rootView.findViewById(R.id.listCategory);

        refreshCategory = (SwipeRefreshLayout) rootView.findViewById(R.id.refreshCategory);

        refreshCategory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadCategory();
            }
        });

        refreshCategory.setRefreshing(true);

        new CategoryLoader().execute();
        return rootView;
    }

    private void reloadCategory() {
        new CategoryLoader().execute();
    }


    public class CategoryLoader extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... voids) {
            WebServices ws = new WebServices();
            String data = "";

            try {
                data = ws.GetDataFromService("category", WebServices.HTTP_GET, null);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return data;
        }

        protected void onProgressUpdate(Void... progress) {

        }

        protected void onPostExecute(String result) {
            //Log.d("called from home", result);
            item = new ArrayList<String>();
            int i;
            try {
                JSONArray jsonArray = new JSONArray(result);
                for (i=0; i < jsonArray.length(); i++){
                    String cat = jsonArray.getString(i);
                    item.add(cat);
                }
            }
            catch (JSONException e){
                Log.e("JSONException", "Error: " + e.toString());
            }

            final ArrayAdapter<String> categories = new ArrayAdapter<String>(rootView.getContext(), android.R.layout.simple_list_item_1, item);
            lv.setAdapter(categories);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(getActivity(), ListCategory.class);
                    intent.putExtra("category",item.get(i));
                    view.getContext().startActivity(intent);

                }
            });

            refreshCategory.setRefreshing(false);
        }
    }

}
