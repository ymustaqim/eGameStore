package egames.egamesstore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by asus on 02/11/16.
 */

public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    public ImageAdapter( Context c){
        mContext = c;
    }

    public int getCount(){
        return mThumbIds.length;
    }

    public Object getItem(int position){
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    //create a new ImageView
    public View getView(int position, View convertView, ViewGroup parent){
        View grid;
        ImageView imageView;
        TextView imageText;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null){
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.grid_single, null);
            imageView = (ImageView) grid.findViewById(R.id.grid_image);
            imageText = (TextView) grid.findViewById(R.id.grid_text);
            imageView.setLayoutParams(new GridView.LayoutParams(85,85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8,8,8,8);
            imageText.setText(game_title[position]);
            imageView.setImageResource(mThumbIds[position]);
        } else{
            imageView = (ImageView) convertView;
        }


        return imageView;

    }

    private Integer[] mThumbIds = {

    };

    private String[] game_title = {
    };

}
