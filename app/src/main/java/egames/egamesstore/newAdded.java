//package egames.egamesstore;
//
//import android.graphics.Rect;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import adapters.GameListAdapter;
//import entity.ItemGame;
//
///**
// * Created by asus on 01/11/16.
// */
//
//public class newAdded extends Fragment {
//
//
//    private LinearLayoutManager lLayout;
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//        View rootView = inflater.inflate(R.layout.new_added, container, false);
//
//
//        lLayout = new LinearLayoutManager(getActivity());
//
//        RecyclerView rView = (RecyclerView) rootView.findViewById(R.id.recAll);
//        rView.setLayoutManager(lLayout);
//
//        GameListAdapter gameAdapter = new GameListAdapter(getActivity(), getGameList());
//        rView.setAdapter(gameAdapter);
//
//        return rootView;
//    }
//
//    private List<ItemGame> getGameList() {
//        List<ItemGame> itemGames = new ArrayList<ItemGame>();
//        itemGames.add(new ItemGame("Arcade", "Action", "http://localhost/img.png", "Neo Contra", "OpenGame", "This game is good", "19 Juni 2017"));
//
//        return itemGames;
//    }
//
//    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
//        private int space;
//
//        public SpacesItemDecoration(int space){
//            this.space = space;
//        }
//
//        @Override
//        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state){
//            outRect.left= space;
//            outRect.right = space;
//            outRect.bottom = space;
//
//            if (parent.getChildLayoutPosition(view)==0){
//                outRect.top = space;
//            }
//            else{
//                outRect.top=0;
//            }
//
//        }
//    }
//
//
//
//
//}
package egames.egamesstore;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import adapters.EndlessRecyclerViewScrollListener;
import adapters.GameListAdapter;
import adapters.WebServices;
import entity.ItemGame;

/**
 * Created by asus on 01/11/16.
 */

public class newAdded extends Fragment {

    private LinearLayoutManager lLayout;
    private List<ItemGame> rowListItem = null;
    private RecyclerView rView;
    private SwipeRefreshLayout refreshGameList;
    private GameListAdapter gameListAdapter;
    private EndlessRecyclerViewScrollListener scrollListener;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.new_added, container, false);


        lLayout = new LinearLayoutManager(getActivity());

        rView = (RecyclerView) rootView.findViewById(R.id.callNewAdded);
        rView.setLayoutManager(lLayout);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        rView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        scrollListener = new EndlessRecyclerViewScrollListener(lLayout) {
            @Override
            public void onScrolled(RecyclerView view, int dx, int dy) {
                super.onScrolled(view, dx, dy);
                rootView.findViewById(R.id.loadItemsLayout_recyclerView).setVisibility(View.VISIBLE);
            }


            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                new GetGameListTask(page).execute();

            }
        };
        rView.addOnScrollListener(scrollListener);

        refreshGameList = (SwipeRefreshLayout) rootView.findViewById(R.id.refreshGameList);

        refreshGameList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadGameList();
            }
        });

        refreshGameList.setRefreshing(true);

        //new AsyncConnection().execute();
        new GetGameListTask(0).execute();


        return rootView;
    }

    private void reloadGameList() {
        gameListAdapter.clearData();
        new GetGameListTask(0).execute();
    }

    public class GetGameListTask extends AsyncTask<Void, Void, List<ItemGame>> {
        private int page;
        private int pageSize=9;
        private int total;

        public GetGameListTask(int page) {
            this.page=page;
        }


        @Override
        protected List<ItemGame> doInBackground(Void... voids) {

            String iresponse = "";
            ArrayList<ItemGame> listGame = new ArrayList<ItemGame>();
            JSONObject rootObj = null;
            JSONArray listData = null;

            // make connection
            WebServices ws = new WebServices();
            try {
                iresponse = ws.GetDataFromService("?from="+Integer.toString(page*pageSize)+"&size="+Integer.toString(pageSize), WebServices.HTTP_GET, null);
                try {
                    rootObj = new JSONObject(iresponse);
                    JSONObject jd = rootObj.getJSONObject("hits");
                    total = jd.getInt("total");
                    listData = jd.getJSONArray("hits");
                    for (int i= 0; i < listData.length(); i++) {
                        JSONObject g = null;
                        g = listData.getJSONObject(i).getJSONObject("_source");
                        ItemGame ig;
                        ig = new ItemGame(g.getString("id").trim(),
                                g.getString("category").trim(),
                                g.getString("img").trim(),
                                g.getString("name").trim(),
                                g.getString("dev").trim(),
                                g.getString("desc").trim(),
                                g.getString("date").trim());
                        Bitmap gb;
                        Object _img = g.get("img");
                        String[] _i = null;
                        if (_img instanceof JSONArray) {
                            JSONArray imgArr = (JSONArray) _img;
                            _i = new String[imgArr.length()];
                            gb = this.loadBitmap("http:"+imgArr.getString(0));
                            // semua gambar disimpan
                            for(int z=0; z < imgArr.length(); z++) {
                                _i[z] = imgArr.getString(z);
                            }
                        } else {
                            _i = new String[1];
                            _i[0] = g.getString("img");
                            gb = this.loadBitmap("http:"+g.getString("img"));
                        }

                        Log.d("from internet: ", Arrays.toString(_i));

                        ig.setPicture(_i);
                        ig.setRate(Float.parseFloat(listData.getJSONObject(i).getString("_score")));
                        ig.setBitmap(gb);
                        listGame.add(ig);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            // return List<ItemGame>
            return listGame;
        }

        protected void onPostExecute(List<ItemGame> res) {
            if (total<=(page+1)*pageSize){
                return;
            }
            if (gameListAdapter==null || page==0){
                // create adapter
                gameListAdapter = new GameListAdapter (getActivity(), res);
                rootView.findViewById(R.id.loadItemsLayout_recyclerView).setVisibility(View.GONE);
                // populate to recyclerview

                rView.setAdapter(gameListAdapter);
                Log.d("Ini refresh","Post data");
            }
            else{
                gameListAdapter.addItemList(res);
                rootView.findViewById(R.id.loadItemsLayout_recyclerView).setVisibility(View.GONE);
            }
            refreshGameList.setRefreshing(false);

            // done.....
        }

        private Bitmap loadBitmap(String url) {
            InputStream in = null;
            Bitmap pict = null;
            try {
                in = new URL(url).openConnection().getInputStream();
                pict = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return pict;
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space){
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state){
            outRect.left= space;
            outRect.right = space;
            outRect.bottom = space;

            if (parent.getChildLayoutPosition(view)==0){
                outRect.top = space;
            }
            else{
                outRect.top=0;
            }

        }
    }




}
