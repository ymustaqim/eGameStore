package egames.egamesstore;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import adapters.WebServices;
import entity.ItemGame;

public class DetailGame extends AppCompatActivity {

    ImageView gamePicture;
    TextView titleDetail;
    TextView descriptionDetail;
    Button btnDownload;
    private DownloadManager downloadManager = null;
    private long lastDownload = -1L;
    ItemGame itemGame;
    AndroidImageAdapter adapterView;

    @Override
    protected void onDestroy() {
        unregisterReceiver(onComplete);
        unregisterReceiver(onNotificationClick);
        super.onDestroy();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_game);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        itemGame = getIntent().getParcelableExtra("itemGame");
        Log.d("ini imagenya : ", Arrays.toString(itemGame.getPicture()));
        Bitmap gamePict = getIntent().getParcelableExtra("gamePicture");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        toolbar.setTitle(itemGame.getName());
        setSupportActionBar(toolbar);

        //find id in layout
        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewPageAndroid);
        adapterView = new AndroidImageAdapter(this);
        mViewPager.setAdapter(adapterView);

        // initialize download manager
        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        registerReceiver(onNotificationClick, new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED));


        gamePicture = (ImageView) findViewById(R.id.imgDetail);
        gamePicture.setImageBitmap(gamePict);

        titleDetail = (TextView) findViewById(R.id.titleDetail);
        titleDetail.setText(itemGame.getName());

        descriptionDetail = (TextView) findViewById(R.id.descriptionDetail);
        descriptionDetail.setText(itemGame.getDesc());
        btnDownload = (Button) findViewById(R.id.downloadDetail);

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDownload(view);
            }
        });

        new PictureTask().execute();

    }

    public class AndroidImageAdapter extends PagerAdapter {
        Context mContext;

        AndroidImageAdapter(Context context) {
            this.mContext = context;
            bitmapPicture = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return bitmapPicture.size();
        }

        private int[] sliderImagesId = new int[]{
                R.drawable.sample_0, R.drawable.sample_1, R.drawable.sample_2,
                R.drawable.sample_3, R.drawable.sample_4, R.drawable.sample_6,
        };

        private List<Bitmap> bitmapPicture;

        public void updatePicture(List<Bitmap> ls) {
            bitmapPicture.clear();

            bitmapPicture.addAll(ls);
            this.notifyDataSetChanged();

        }

        @Override
        public boolean isViewFromObject(View v, Object obj) {
            return v == ((ImageView) obj);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int i) {
            ImageView mImageView = new ImageView(mContext);
            mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            mImageView.setImageBitmap(bitmapPicture.get(i));
            ((ViewPager) container).addView(mImageView, 0);
            return mImageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int i, Object obj) {
            ((ViewPager) container).removeView((ImageView) obj);
        }
    }

    public class PictureTask extends AsyncTask <Void, Void, List<Bitmap>>
    {

        @Override
        protected void onPostExecute(List<Bitmap> bitmaps) {

            DetailGame.this.adapterView.updatePicture(bitmaps);
        }

        @Override
        protected List<Bitmap> doInBackground(Void... voids) {

            List<Bitmap> picture = new ArrayList<>();

            String urls[] = DetailGame.this.itemGame.getPicture();

            for(String url : urls) {
                Log.d("ngelog : ", url);
                Bitmap pct = loadBitmap("http:" + url);
                picture.add(pct);
            }

            return picture;
        }

        private Bitmap loadBitmap(String url) {
            InputStream in = null;
            Bitmap pict = null;
            try {
                in = new URL(url).openConnection().getInputStream();
                pict = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return pict;
        }
    }

    private void startDownload(View v) {
        Uri uri = Uri.parse(WebServices.HTTP_URL+"download?category="+itemGame.getCategory()+"&id="+itemGame.getId());

        Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .mkdirs();

        lastDownload = downloadManager.enqueue(new DownloadManager.Request(uri)
                    .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                    .setAllowedOverRoaming(false)
                    .setDescription("Downloaded game")
                    .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, itemGame.getName()+".apk"));
        v.setEnabled(false);

    }

    private String statusMessage(Cursor c) {
        String msg = "???";

        switch (c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
            case DownloadManager.STATUS_FAILED:
                msg = "Download failed!";
                break;
            case DownloadManager.STATUS_PAUSED:
                msg = "Download paused!";
                break;
            case DownloadManager.STATUS_PENDING:
                msg = "Download pending!";
                break;
            case DownloadManager.STATUS_RUNNING:
                msg = "Download running!";
                break;
            case DownloadManager.STATUS_SUCCESSFUL:
                msg = "Download complete!";
                break;
            default:
                msg = "There is no file download";
                break;
        }

        return msg;
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            findViewById(R.id.downloadDetail).setEnabled(true);
            Cursor c = downloadManager.query(new DownloadManager.Query().setFilterById(lastDownload));

            if (c == null) {
                Toast.makeText(context, "Download not found!", Toast.LENGTH_LONG).show();
            } else {
                c.moveToFirst();

                Toast.makeText(context, statusMessage(c), Toast.LENGTH_LONG).show();
            }
        }
    };

    BroadcastReceiver onNotificationClick = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            Toast.makeText(context, "DOwnloading.....", Toast.LENGTH_LONG).show();
            Cursor c = downloadManager.query(new DownloadManager.Query().setFilterById(lastDownload));

            if (c == null) {
                Toast.makeText(context, "Download not found!", Toast.LENGTH_LONG).show();
            } else {
                c.moveToFirst();

                Toast.makeText(context, statusMessage(c), Toast.LENGTH_LONG).show();
            }
        }
    };

}
